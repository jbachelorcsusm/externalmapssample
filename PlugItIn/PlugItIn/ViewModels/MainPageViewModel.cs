﻿using Plugin.ExternalMaps;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PlugItIn.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public DelegateCommand MapTheKillerCookieCommand { get; set; }
        public DelegateCommand HitTheSlopesCommand { get; set; }

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Plug-Ins";

            MapTheKillerCookieCommand = new DelegateCommand(MapTheKillerCookie);
            HitTheSlopesCommand = new DelegateCommand(HitTheSlopes);
        }

        private void HitTheSlopes()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HitTheSlopes)}");

            CrossExternalMaps.Current.NavigateTo("Beaver Creek", "210 Beaver Creek Plaza", "Beaver Creek", "CO", "81620", "USA", "USA");
        }

        private void MapTheKillerCookie()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MapTheKillerCookie)}");

            CrossExternalMaps.Current.NavigateTo("Epoch", "1231 Elfin Forest Rd #110", "San Marcos", "CA", "92078", "USA", "USA");
        }
    }
}
